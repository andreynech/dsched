package main

import (
	"flag"
	ds "gitlab.com/andreynech/dsched/proto"
	"log"
	"os"
	"testing"
)

func init() {
	taskDBFileName = flag.String("i", os.TempDir()+"/dscheduler.db", "DB file")
	flag.Parse()
	log.Printf("Task list stored in %s", *taskDBFileName)
}

func TestAddImpl(t *testing.T) {

	t1, e1 := addImpl(&ds.Task{Id: 0, Cron: "@hourly", Action: ""})

	if e1 != nil || len(globalTaskList.GetTasks()) != 1 {
		t.Errorf("Error adding second task. Err: %s len: %d Id: %d Tasks: %v",
			e1, len(globalTaskList.GetTasks()), t1.Id, globalTaskList.GetTasks())
	}

	t2, e2 := addImpl(&ds.Task{Id: 0, Cron: "@hourly", Action: ""})
	if e2 != nil || len(globalTaskList.GetTasks()) != 2 {
		t.Errorf("Error adding second task. Err: %s len: %d Id: %d Tasks: %v",
			e2, len(globalTaskList.GetTasks()), t2.Id, globalTaskList.GetTasks())
	}
}

func TestRemoveAllImpl(t *testing.T) {

	addImpl(&ds.Task{Id: 0, Cron: "@hourly", Action: ""})
	addImpl(&ds.Task{Id: 0, Cron: "@hourly", Action: ""})
	err := removeAllImpl()
	if err != nil || len(globalTaskList.GetTasks()) != 0 {
		t.Errorf("Error removing all tasks. Err: %s len: %d Tasks: %v",
			err, len(globalTaskList.GetTasks()), globalTaskList.GetTasks())
	}
}

func TestRemoveImpl(t *testing.T) {
	removeAllImpl()
	addImpl(&ds.Task{Id: 0, Cron: "@hourly", Action: ""})
	second, _ := addImpl(&ds.Task{Id: 0, Cron: "@hourly", Action: ""})
	third, _ := addImpl(&ds.Task{Id: 0, Cron: "@hourly", Action: ""})
	err := removeImpl(&ds.Task{Id: second.GetId(), Cron: "", Action: ""})
	if err != nil || len(globalTaskList.GetTasks()) != 2 || globalTaskList.GetTasks()[1].Id != third.GetId() {
		t.Errorf("Error removing second task. Err: %s len: %d Tasks: %v",
			err, len(globalTaskList.GetTasks()), globalTaskList.GetTasks())
	}
}

func TestListImpl(t *testing.T) {
	removeAllImpl()
	addImpl(&ds.Task{Id: 0, Cron: "@hourly", Action: ""})
	addImpl(&ds.Task{Id: 0, Cron: "@hourly", Action: ""})
	third, _ := addImpl(&ds.Task{Id: 0, Cron: "@hourly", Action: ""})
	list, err := listImpl()
	if err != nil || len(list.GetTasks()) != 3 || list.GetTasks()[2].Id != third.GetId() {
		t.Errorf("Error listing tasks. Err: %s len: %d Tasks: %v",
			err, len(list.GetTasks()), list.GetTasks())
	}
}
