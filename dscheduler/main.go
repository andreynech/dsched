package main

import (
	"flag"
	ds "gitlab.com/andreynech/dsched/proto"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"log"
	"net"
)

// server is used to implement dsched.Scheduler
type server struct{}

var (
	port           *string
	taskDBFileName *string
)

func (s *server) List(ctx context.Context, req *ds.Empty) (*ds.TaskList, error) {
	response, error := listImpl()
	if error != nil {
		log.Fatalf("Error processing List request: %v", error)
	}
	return response, error
}

func (s *server) Add(ctx context.Context, req *ds.Task) (*ds.Task, error) {
	response, error := addImpl(req)
	if error != nil {
		log.Fatalf("Error processing Add request: %v", error)
	}
	return response, error
}

func (s *server) Remove(ctx context.Context, req *ds.Task) (*ds.Empty, error) {
	error := removeImpl(req)
	if error != nil {
		log.Fatalf("Error processing Remove request: %v", error)
	}
	return &ds.Empty{}, error
}

func (s *server) RemoveAll(ctx context.Context, req *ds.Empty) (*ds.Empty, error) {
	error := removeAllImpl()
	if error != nil {
		log.Fatalf("Error processing RemoveAll request: %v", error)
	}
	return &ds.Empty{}, error
}

func main() {
	// Parse command line parameters
	port = flag.String("p", ":50051", "Endpoint to listen")
	taskDBFileName = flag.String("i", "/var/run/dscheduler.db", "File name to store task list")
	flag.Parse()

	tasklist, err := loadTaskList(taskDBFileName)
	if err != nil {
		log.Printf("failed to load task list: %v", err)
	}
	globalTaskList = tasklist

	log.Println("Loaded schedule:")
	for _, v := range globalTaskList.GetTasks() {
		log.Printf("%d %s  ->  %s", v.GetId(), v.GetCron(), v.GetAction())
	}

	lis, err := net.Listen("tcp", *port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	jobrunner.Start()

	s := grpc.NewServer()
	ds.RegisterSchedulerServer(s, &server{})
	// Register reflection service on gRPC server.
	reflection.Register(s)
	log.Println("Server started")
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}

	jobrunner.Stop()
}
