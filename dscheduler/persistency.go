package main

import (
	"github.com/golang/protobuf/proto"
	ds "gitlab.com/andreynech/dsched/proto"
	"io/ioutil"
)

func loadTaskList(filename *string) (*ds.TaskList, error) {
	// Read input file
	in, err := ioutil.ReadFile(*filename)
	if err != nil {
		return &ds.TaskList{}, err
	}

	// Parse file content into TaskList
	list := &ds.TaskList{}
	if err = proto.Unmarshal(in, list); err != nil {
		return &ds.TaskList{}, err
	}

	for i, v := range list.GetTasks() {
		cmd := command{cmdLine: v.GetAction()}
		entryID, err := jobrunner.AddJob(v.GetCron(), cmd)
		if err != nil {
			return &ds.TaskList{}, err
		}
		list.GetTasks()[i].Id = uint32(entryID)
	}

	return list, nil
}

func saveTaskList(list *ds.TaskList) error {
	// Serialize
	data, err := proto.Marshal(list)
	if err != nil {
		return err
	}

	// Write output file
	err = ioutil.WriteFile(*taskDBFileName, data, 0644)
	if err != nil {
		return err
	}

	return nil
}
