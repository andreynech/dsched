#!/usr/bin/env python

import grpc
import dsched_pb2 as ds
import dsched_pb2_grpc
from optparse import OptionParser


def run():

    parser = OptionParser()

    parser.add_option('-e', '--endpoint',
        dest='endpoint', default='localhost:50051',
        help='Host:port ENDPOINT to connect',
        metavar='ENDPOINT')

    parser.add_option('-l', '--list',
        action="store_true", dest='list', default=False,
        help='List scheduled tasks')

    parser.add_option('-s', '--schedule',
        action="store_true", dest='schedule', default=False,
        help='Schedule task. --cron and --action arguments are required in this case.')

    parser.add_option('-c', '--cron', dest='cron',
        help='CRON statement which describes when to execute the command',
        metavar='CRON')

    parser.add_option('-a', '--action',
        dest='action',
        help='ACTION is the command to execute at time specified by --cron argument',
        metavar='ACTION')

    parser.add_option('-r', '--remove',
        dest='remove', default=0, type='int',
        help='Removes TASKID from schedule',
        metavar='TASKID')

    parser.add_option('-p', '--purge',
        action="store_true", dest='purge', default=False,
        help='Purge all scheduled tasks')

    (options, args) = parser.parse_args()

    with grpc.insecure_channel(options.endpoint) as channel:
        scheduler = dsched_pb2_grpc.SchedulerStub(channel)
        print('Connected to', options.endpoint)

        if options.list:
            response = scheduler.List(ds.Empty())
            print("Scheduled entities:")
            for e in response.tasks:
                print(e.id, e.cron, e.action)

        if options.schedule:
            if options.cron is None:
                print('--cron option is required to schedule task')
            else:
                if options.action is None:
                    print('--action option is required to schedule task')
                else:
                    # Task ID is ignored, hence id=0 here.
                    # Returned task has properly initialized ID
                    task = ds.Task(id=0, cron=options.cron, action=options.action)
                    entity = scheduler.Add(task)
                    print('Task scheduled with ID:', entity.id)

        if options.remove != 0:
            # Only task ID matters here. All other attributes are ignored
            task = ds.Task(id=options.remove, cron='', action='')
            scheduler.Remove(task)
            print('Task removed')

        if options.purge:
            scheduler.RemoveAll(ds.Empty())
            print('All tasks removed')


if __name__ == '__main__':
    run()
